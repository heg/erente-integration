<?php include 'partials/header.php' ?>


    <!-- Main content start -->
    <div class="main-content">
        <div class="container">

            <h2>Estimation des rentes de vieillesse <span class="text-bold">avec changement de revenu</span></h2>

            <div class="row">
                <div class="content col-md-8">
                    <div class="description">Veuillez saisir les informations vous concernant</div>
                    <form>
                        <div class="field-group date-de-naissance">
                            <div class="field">
                                <label for="date-de-naissance">Votre date de naissance</label>
                                <input type="date" name="date-de-naissance" id="date-de-naissance"/>
                            </div>
                            <div class="user-no-info"><a href="#" onclick="alert('Cette information est indispensable à la simulation');">> Je ne dispose pas de l'information</a></div>
                        </div>
                        <div class="field-group assure-avs">
                            <div class="field">
                                <label for="assure-avs">Assuré à l'AVS</label>
                                <select type="select" name="assure-avs" id="assure-avs" placeholder="Oui">
                                    <option value="oui">Oui</option>
                                    <option value="oui-toujours">Oui, toujours</option>
                                    <option value="non">Non</option>
                                </select>
                            </div>
                            <div class="user-no-info"><a href="#" onclick="alert('Cette information est indispensable à la simulation');">> Je ne dispose pas de l'information</a></div>
                        </div>
                        <div class="field-group revenu-annuel">
                            <div class="field">
                                <label for="revenu-annuel">Revenu annuel brut suisse</label>
                                <input type="text" name="revenu-annuel" id="revenu-annuel" placeholder="100'000"/>
                            </div>
                            <div class="user-no-info"><a href="#" onclick="alert('Cette information est indispensable à la simulation');">> Je ne dispose pas de l'information</a></div>
                        </div>
                    </form>
                </div>
                <div class="actions col-md-4">
                    <div class="boutons">
                        <button class="back">Revenir à l'étape précédente</button>
                        <button class="next validation">Valider les informations</button>
                    </div>
                </div>
            </div>
        </div>  
    </div>
    <!-- Main content end -->

<?php include 'partials/footer.php' ?>